(define
    (domain pacman_hard)
    (:requirements :strips :typing :equality :adl)

    (:types
        pos
    )

    (:predicates
        (At ?p -pos)
        (FoodAt ?p -pos)
        (GhostAt ?p -pos)
        (CapsuleAt ?p -pos)
        (Adjacent ?pos1 ?pos2 -pos)
        (Powered)
        (count1)
    )


    (:action move
        :parameters (?posCurr ?posNext -pos)
        :precondition (and
            (At ?posCurr)
            (Adjacent ?posCurr ?posNext)
            (or 
                (and
                    (GhostAt ?posNext)
                    
                    (exists (?p - pos)
                        (FoodAt ?p) )
                    (or
                        (Powered) (count1) ) )
                (not (GhostAt ?posNext))    )
        )
        :effect (and
        (when (CapsuleAt ?posNext)
        (and 
            (not (CapsuleAt ?posNext)) 
            (Powered) 
            (not(count1))  ) )
        (At ?posNext)
        (not(At ?posCurr))
        (when (FoodAt ?posNext)
        (not (FoodAt ?posNext)) )
        (when (Powered) 
        (and (not (Powered)) (count1) (not (GhostAt ?posNext)) ) )
        (when (count1) 
        (and (not (count1)) (not (GhostAt ?posNext)) ) )
        )
        
    )
)