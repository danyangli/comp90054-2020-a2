(define
    (domain pacman_mid)
    (:requirements :strips :typing :equality :adl)
    (:types pos)
    (:predicates
        (At ?p -pos)
        (FoodAt ?p -pos)
        (GhostAt ?p -pos)
        (CapsuleAt ?p -pos)
        (Adjacent ?pos1 ?pos2 -pos)
        (Powered)
    )
    (:action move
        :parameters (?posCurr ?posNext -pos)
        :precondition (and
            (At ?posCurr)
            (Adjacent ?posCurr ?posNext)
            (or (and
                (GhostAt ?posNext)
                (Powered) )
            (not (GhostAt ?posNext))    )
        )
        :effect (and
        (when (CapsuleAt ?posNext)
        (and (not (CapsuleAt ?posNext)) (Powered) ) )

        (At ?posNext)
        (not(At ?posCurr))

        (when (FoodAt ?posNext)
        (not (FoodAt ?posNext)) )

        (when (Powered) 
        (not (GhostAt ?posNext)) )
        )
    )
)


