(define
    (domain pacman_simple)
    (:requirements :strips :typing :equality :adl)

    (:types pos
    )

    (:predicates 
        (At ?p -pos)
        (FoodAt ?p -pos)
        (GhostAt ?p -pos)
        (CapsuleAt ?p -pos)
        (Adjacent ?pos1 ?pos2 -pos)
    )


    (:action move
        :parameters (?posCurr ?posNext -pos)
        :precondition (and 
                (At ?posCurr)
                (Adjacent ?posCurr ?posNext)
                (not (GhostAt ?posNext))
        )
        :effect (and (At ?posNext)
        (not (At ?posCurr))
        (not (FoodAt ?posNext))
        (not (CapsuleAt ?posNext))
        )
    )
)