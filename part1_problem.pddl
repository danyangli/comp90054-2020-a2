(define
    (problem pacman-level-1)
    (:domain pacman_simple)

;; problem map
;;  | 1 | 2 | 3 |
;; -|---|---|---|
;; a| P | G | F | 
;; b| _ | _ | _ | 
;;  |---|---|---| 

  



    (:objects a1 a2 a3  b1 b2 b3 - pos
	)
	
	(:init
	(Adjacent a1 a2) (Adjacent a2 a1)
	(Adjacent a2 a3) (Adjacent a3 a2)
; 	(Adjacent a3 a4) (Adjacent a4 a3)
; 	(Adjacent a4 a5) (Adjacent a5 a4)
	
	(Adjacent b1 b2) (Adjacent b2 b1)
	(Adjacent b2 b3) (Adjacent b3 b2)
; 	(Adjacent b3 b4) (Adjacent b4 b3)
; 	(Adjacent b4 b5) (Adjacent b5 b4)
	
	(Adjacent a1 b1) (Adjacent b1 a1)
	(Adjacent a2 b2) (Adjacent b2 a2)
	(Adjacent a3 b3) (Adjacent b3 a3)
; 	(Adjacent a4 b4) (Adjacent b4 a4)
; 	(Adjacent a5 b5) (Adjacent b5 a5)
	
	(At a1) 
	(FoodAt a3)
	(GhostAt a2)
	)

    (:goal
    (forall (?p - pos)
        (not (FoodAt ?p) )
    )
    )
)